//For exit()
#include <stdlib.h>
//Include SDL library
#include "SDL.h"
//Include SDL_image this library provides
//support for loading different types of images.
#include "SDL_image.h"


const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 600;



int main(int argc, char* args[])
{
    // Declare window and renderer objects
    SDL_Window* gameWindow = nullptr;
    SDL_Renderer* gameRenderer = nullptr;
    // Temporary surface used while loading the image
    SDL_Surface* temp = nullptr;
    // Texture which stores the actual sprite (this
    // will be optimised).
    SDL_Texture* backgroundTexture = nullptr;
    SDL_Texture* playerTexture = nullptr;

    // Player Image - source rectangle
   // - The part of the texture to render.
    SDL_Rect sourceRectangle;
    // - Where to render the texture
    SDL_Rect targetRectangle;
    // Rotation
    float angle;
    // Flipping
    SDL_RendererFlip flip;


        // SDL allows us to choose which SDL components are going to
        // be initialised.
        SDL_Init(SDL_INIT_EVERYTHING);
        gameWindow = SDL_CreateWindow("Hello Friends", // Window title
            SDL_WINDOWPOS_UNDEFINED, // X position
            SDL_WINDOWPOS_UNDEFINED, // Y position
            WINDOW_WIDTH, // width
            WINDOW_HEIGHT, // height
            SDL_WINDOW_SHOWN); // Window flags
            // if the window creation succeeded create our renderer
        gameRenderer = SDL_CreateRenderer(gameWindow, 0, 0);


        /*************************
        * Setup background image *
        * ************************/
        // Load the sprite to our temp surface
        temp = IMG_Load("assets/images/background.png");
        // Create a texture object from the loaded image
        backgroundTexture =
            SDL_CreateTextureFromSurface(gameRenderer, temp);
        // Clean-up - we are done with "temp" now our texture
        // has been created
        SDL_FreeSurface(temp);
        temp = nullptr;




    /**************************
    * Setup player texture *
    ***************************/
    // Load the sprite to our temp surface
    temp = SDL_LoadBMP("assets/images/dorf2.png");
    // Create a texture object from the loaded image
    playerTexture =
        SDL_CreateTextureFromSurface(gameRenderer, temp);
    // Clean-up - we are done with "temp" now
    SDL_FreeSurface(temp);
    temp = nullptr;


    //Setup source and destination rects
    sourceRectangle.x = 0;
    sourceRectangle.y = 0;
    // Use the texture info to get the to get the
    // rectangle size.
    // We are using the whole of the sprite image!
    SDL_QueryTexture(playerTexture, 0, 0,
        &(sourceRectangle.w), //width
        &(sourceRectangle.h)); //height


    
// I worked this out via trial and error.
    targetRectangle.x = 100;
    targetRectangle.y = 400;
    // We could choose any size/scale
    targetRectangle.w = sourceRectangle.w * 1.0f;
    targetRectangle.h = sourceRectangle.h * 1.0f;
    // Flip both horizontally and vertically
    // Should work -- But these are enums
    // The | is applied to the 2 enums as uints and must be cast back.
    flip = static_cast<SDL_RendererFlip>(SDL_FLIP_HORIZONTAL |
        SDL_FLIP_VERTICAL);
    // Set the angle
    angle = 45.0f;



    // 1. Clear the screen (black)
    SDL_SetRenderDrawColor(gameRenderer, 0, 0, 0, 255);
    // Colour provided as red, green, blue and alpha
    //(transparency) values (i.e. RGBA)
    SDL_RenderClear(gameRenderer);


    // 2. Draw the scene
    SDL_RenderCopyEx(gameRenderer,
        playerTexture,
        &sourceRectangle,
        &targetRectangle,
        angle,
        NULL,
        flip);

    SDL_RenderCopy(gameRenderer, // where to render
        playerTexture, // what to render
        &sourceRectangle, // source
        &targetRectangle); // destination

// 3. Present the current frame to the screen
    SDL_RenderPresent(gameRenderer);


    //Pause to allow the image to be seen
    SDL_Delay(10000);


    //Clean up!
    SDL_DestroyTexture(playerTexture);
    SDL_DestroyTexture(backgroundTexture);
    SDL_DestroyRenderer(gameRenderer);
    SDL_DestroyWindow(gameWindow);
    //Shutdown SDL - clear up resources etc.
    SDL_Quit();
    // Exit the program
    exit(0);
}